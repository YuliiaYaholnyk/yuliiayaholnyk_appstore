import 'package:flutter/material.dart';

class CardModel {
  final String category;
  final String description;
  final String title;
  final String image;

  CardModel({
    @required this.title,
    @required this.category,
    @required this.description,
    @required this.image,
  });
}
