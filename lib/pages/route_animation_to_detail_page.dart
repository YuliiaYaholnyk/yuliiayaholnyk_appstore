import 'package:AppStore/models/model.dart';
import 'package:AppStore/pages/detail_page.dart';
import 'package:flutter/material.dart';

void routeAnimationToDetailPage(BuildContext context, CardModel data, int i) {
  Navigator.of(context).push(
    PageRouteBuilder(
      fullscreenDialog: true,
      transitionDuration: Duration(milliseconds: 500),
      pageBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
          ) {
        return DetailPage(data, i);
      },
      transitionsBuilder: (
          BuildContext context,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
          Widget child,
          ) {
        var begin = Offset(0.0, -0.1);
        var end = Offset(0.0, 0.0);
        var curve = Curves.linear;
        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    ),
  );
}