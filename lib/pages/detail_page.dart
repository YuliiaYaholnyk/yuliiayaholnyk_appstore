import 'file:///C:/Users/AppVesto/development/projects/AppStore/AppStore/lib/widgets/about_floating_container.dart';
import 'package:AppStore/consts/const.dart';
import 'package:AppStore/widgets/bottom_info_container.dart';
import 'package:AppStore/widgets/card_content.dart';
import 'package:AppStore/widgets/detail_title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../models/model.dart';

class DetailPage extends StatefulWidget {
  final CardModel data;
  final int index;

  DetailPage(this.data, this.index);

  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  ScrollController _controller;
  bool _visible = false;

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          controller: _controller,
          child: Column(
            children: <Widget>[
              DetailTitle(widget: widget),
              Stack(
                children: <Widget>[
                  CardContent(widget: widget),
                ],
              ),
            ],
          ),
        ),
        _visible
            ? Positioned(
                bottom: 20.0,
                child: AboutFloatingContainer(),
              )
            : Container(),
      ],
    );
  }

  _scrollListener() {
    if (_controller.offset > (_controller.position.maxScrollExtent - 300.0) && !_controller.position.outOfRange) {
      setState(() {
        _visible = false;
      });
    } else if (_controller.offset > (_controller.position.minScrollExtent + 400.0) && !_controller.position.outOfRange) {
      setState(() {
        _visible = true;
      });
    } else if (_controller.offset < (_controller.position.minScrollExtent + 400.0) && !_controller.position.outOfRange) {
      setState(() {
        _visible = false;
      });
    }
  }

}
