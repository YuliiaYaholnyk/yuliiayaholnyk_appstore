import 'package:AppStore/consts/const.dart';
import 'package:AppStore/widgets/card_widget.dart';
import 'package:flutter/material.dart';

import '../models/model.dart';
class CardList extends StatefulWidget {
  @override
  _CardListState createState() => _CardListState();
}

class _CardListState extends State<CardList> {
  final data = CardModel(
    title: 'Wilmost`s Warehouse',
    category: 'GAME OF THE DAY',
    description: 'Puzzle',
    image: IMAGE,
  );

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: ListView.builder(
          itemCount: 5,
          itemBuilder: (BuildContext ctx, int i) {
            return CardWidget(
              data: data,
              i: i,
            );
          }),
    );
  }
}
