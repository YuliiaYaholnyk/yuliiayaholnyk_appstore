import 'package:AppStore/consts/const.dart';
import 'package:AppStore/pages/detail_page.dart';
import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
class DetailTitle extends StatelessWidget {
  const DetailTitle({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final DetailPage widget;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragEnd: (_) {
        Navigator.of(context).pop();
      },
      child: Stack(
        children: <Widget>[
          Hero(
            tag: 'tag${widget.index}',
            child: Image(
              image: AssetImage(widget.data.image),
            ),
          ),
          Positioned(
              right: 10.0,
              bottom: 60.0,
              child: Column(
                children: <Widget>[
                  Hero(
                    tag: 'category${widget.index}',
                    child: Material(
                      color: Colors.transparent,
                      child: Container(
                          height: 90,
                          width: 110,
                          child: AutoSizeText(
                            widget.data.category,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontSize: 30.0,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              height: 0.8,
                            ),
                          )),
                    ),
                  ),
                ],
              )),
          Positioned(
            bottom: 12.0,
            child: Container(
              margin: EdgeInsets.only(left: 25.0),
              width: 310,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Hero(
                    tag: 'price${widget.index}',
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(20.0),
                        ),
                      ),
                      width: 55,
                      height: 20,
                      child: Center(
                        child: Material(
                          color: Colors.transparent,
                          child: Text(
                            '\$4.99',
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.lightBlue[900],
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Hero(
                        tag: 'title${widget.index}',
                        child: Container(
                          child: Center(
                            child: Material(
                              color: Colors.transparent,
                              child: Text(
                                widget.data.title,
                                style: TextStyle(
                                  fontSize: 12.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Hero(
                        tag: 'description${widget.index}',
                        child: Material(
                          color: Colors.transparent,
                          child: Text(
                            widget.data.description,
                            style: TextStyle(
                              fontSize: 10.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Hero(
                    tag: 'image${widget.index}',
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0),
                        ),
                      ),
                      width: 35.0,
                      height: 35.0,
                      child: Image(
                        image: AssetImage(LOGO),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
