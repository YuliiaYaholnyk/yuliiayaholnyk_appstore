import 'package:AppStore/consts/const.dart';
import 'package:flutter/material.dart';

import '../models/model.dart';

class AboutFloatingContainer extends StatefulWidget {
  @override
  _AboutFloatingContainerState createState() => _AboutFloatingContainerState();
}

class _AboutFloatingContainerState extends State<AboutFloatingContainer> {
  final data = CardModel(
    title: 'Wilmost`s Warehouse',
    category: 'GAME OF THE DAY',
    description: 'Puzzle',
    image: IMAGE,
  );
  double heigth = 30.0;

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 100)).then((value) {
      setState(() {
        heigth = 50.0;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 200),
      decoration: BoxDecoration(
        color: Color.fromRGBO(229, 241, 251, 1),
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      margin: EdgeInsets.only(left: 15.0),
      width: 330.0,
      height: heigth,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
            width: 55,
            height: 20,
            child: Center(
              child: Material(
                color: Colors.transparent,
                child: Text(
                  '\$4.99',
                  style: TextStyle(
                    fontSize: 12,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Material(
                  color: Colors.transparent,
                  child: Text(
                    data.title,
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Material(
                color: Colors.transparent,
                child: Text(
                  data.description,
                  style: TextStyle(fontSize: 10.0, color: Colors.black),
                ),
              ),
            ],
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(8.0),
              ),
            ),
            width: 35.0,
            height: 35.0,
            child: Image(
              image: AssetImage(LOGO),
              fit: BoxFit.fill,
            ),
          ),
        ],
      ),
    );
  }
}
