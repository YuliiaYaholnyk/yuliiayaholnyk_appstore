import 'package:AppStore/models/model.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:AppStore/pages/route_animation_to_detail_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'info_container.dart';

class CardWidget extends StatelessWidget {
  const CardWidget({
    Key key,
    @required this.data,
    @required this.i,
  }) : super(key: key);
  final int i;
  final CardModel data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => routeAnimationToDetailPage(context, data, i),
      child: Card(
        margin: EdgeInsets.all(10.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(17.0),
        ),
        clipBehavior: Clip.antiAlias,
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Hero(
                  tag: 'tag$i',
                  child: Image(
                    image: AssetImage(data.image),
                  ),
                ),
                Positioned(
                  right: 10.0,
                  bottom: 60.0,
                  child: Hero(
                    tag: 'category$i',
                    child: Container(
                      height: 90.0,
                      width: 110.0,
                      child: Material(
                        color: Colors.transparent,
                        child: AutoSizeText(
                          data.category,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontSize: 30.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            height: 0.8,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 12.0,
                  child: InfoContainer(
                    i: i,
                    data: data,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
