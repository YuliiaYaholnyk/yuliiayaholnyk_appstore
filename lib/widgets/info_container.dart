import 'package:AppStore/consts/const.dart';
import 'package:AppStore/models/model.dart';
import 'package:flutter/material.dart';

class InfoContainer extends StatelessWidget {
  const InfoContainer({
    Key key,
    @required this.i,
    @required this.data,
  }) : super(key: key);

  final int i;
  final CardModel data;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 15.0),
      width: 310,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Hero(
            tag: 'price$i',
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
              width: 55,
              height: 20,
              child: Center(
                child: Material(
                  color: Colors.transparent,
                  child: Text(
                    '\$4.99',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.lightBlue[900],
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Hero(
                tag: 'title$i',
                child: Container(
                  child: Material(
                    color: Colors.transparent,
                    child: Text(
                      data.title,
                      style: TextStyle(
                        fontSize: 12.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              Hero(
                tag: 'description$i',
                child: Material(
                  color: Colors.transparent,
                  child: Text(
                    data.description,
                    style: TextStyle(
                      fontSize: 10.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Hero(
            tag: 'image$i',
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
              ),
              width: 35.0,
              height: 35.0,
              child: Image(
                image: AssetImage(LOGO),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
