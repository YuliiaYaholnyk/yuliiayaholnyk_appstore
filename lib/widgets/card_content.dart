import 'package:AppStore/consts/const.dart';
import 'package:AppStore/pages/detail_page.dart';
import 'package:flutter/material.dart';

import 'bottom_info_container.dart';

class CardContent extends StatelessWidget {
  const CardContent({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final DetailPage widget;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Container(
            child: Material(
              color: Colors.transparent,
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  CONTENT_TEXT,
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 15.0,
                  ),
                ),
              ),
            ),
          ),
          BottomInfoContainer(widget: widget)
        ],
      ),
    );
  }
}
