import 'package:AppStore/consts/const.dart';
import 'package:AppStore/pages/detail_page.dart';
import 'package:flutter/material.dart';

class BottomInfoContainer extends StatelessWidget {
  const BottomInfoContainer({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final DetailPage widget;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(229, 241, 251, 0.5),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 20.0, bottom: 10.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(15.0),
              ),
              border: Border.all(
                color: Colors.grey,
                width: 1.0,
              ),
            ),
            width: 70.0,
            height: 70.0,
            child: Image(
              image: AssetImage(LOGO),
              fit: BoxFit.fill,
            ),
          ),
          Material(
            color: Colors.transparent,
            child: Text(
              widget.data.title,
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.black,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Container(
            child: Material(
              color: Colors.transparent,
              child: Text(
                widget.data.description,
                style: TextStyle(
                  fontSize: 15.0,
                  color: Colors.grey,
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10.0, bottom: 20.0),
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
            width: 65,
            height: 25,
            child: Center(
              child: Material(
                color: Colors.transparent,
                child: Text(
                  '\$4.99',
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
